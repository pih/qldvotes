// CHARTS

// overall options
var defaults = function (name) {
  return {
    animation: false,
    title: {
      display: true,
      text: name
    }, legend: {
      display: false
    }, scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };
};

// income
var chart_weekly_income = function (props) {
  // define canvas (which is generated in mapping.js)
  var cv_wi = document.getElementById("cv_wi").getContext('2d');
  // define chart
  var da_wi = [props.income_1, props.income_150, props.income_300,
          props.income_400, props.income_500, props.income_650,
          props.income_800, props.income_100, props.income_125,
          props.income_150, props.income_175, props.income_200,
          props.income_302]
  var ch_wi = new Chart(cv_wi, {
    type: 'bar',
    data: {
      labels : ["0-149", "150-299", "300-399", "400-499", "500-649",
        "650-799", "800-999", "1000-1249", "1250-1499", "1500-1749",
        "1750-1999", "2000-2999", ">3000"],
      datasets: [{
        data: da_wi,
        backgroundColor: barColours(da_wi),
        hoverBackgroundColor: barColours(da_wi)
      }]
    },
    options: defaults("Weekly personal income")
  });
}

// age
var chart_age = function (props) {
  var cv_a = document.getElementById("cv_a").getContext('2d');
  var da_a = [props.age_15, props.age_20, props.age_25, props.age_35, props.age_45,
               props.age_55, props.age_65, props.age_75, props.age_85];
  var ch_a = new Chart(cv_a, {
    type: 'bar',
    data: {
      labels : ["15-19", "20-24", "25-34", "35-44", "45-54", "55-64", "65-74",
                                  "75-84", ">85"],
      datasets: [{
        data: da_a,
        backgroundColor: barColours(da_a),
        hoverBackgroundColor: barColours(da_a)
      }]
    },
    options: defaults("Age")
  });
}

// education
var chart_education = function (props) {
  var cv_e = document.getElementById("cv_e").getContext('2d');
  var ch_e = new Chart(cv_e, {
    type: 'bar',
    data: {
      labels : ["Year 12", "Year 11", "Year 10", "Year 9", "Year 8", "No school", "Not stated"],
      datasets: [{
        label: "Male",
        data: [props.education_12_m, props.education_11_m, props.education_10_m,
          props.education_9_m,
          props.education_8_m, props.education_none_m, props.education_notst_m],
        backgroundColor: "rgba(55, 160, 225, 0.7)",
        hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
        hoverBorderWidth: 2
      },
        {
          label: "Female",
          data: [props.education_12_f, props.education_11_f, props.education_10_f,
            props.education_9_f,
            props.education_8_f, props.education_none_f, props.education_notst_f],
          backgroundColor: "rgba(225, 58, 55, 0.7)",
          backgroundColor: "rgba(225, 58, 55, 0.7)",
          hoverBorderWidth: 2
        }]
    },
    options: {
      title: {
      display: true,
      text: "Level of education"
    }, animation: false,
      legend: {
        display: true,
        position: "bottom"
      }, scales: {
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: true,
          }
        }],
        xAxes: [{
          stacked: true,
        }]
      }
    }
  });
};

// Type of housing
var chart_housing = function (props) {
  var cv_hous = document.getElementById("cv_hous").getContext('2d');
  var da_hous = [props.housing_se, props.housing_s2, props.housing_fl];
  var ch_hous = new Chart(cv_hous, {
    type: 'bar',
    data: {
      labels: [["Separate", "housing"], ["Semi-", "detatched", "housing"],
        ["Flats", "and", "appartments"]],
      datasets: [{
        data: da_hous,
        backgroundColor: barColours(da_hous),
        hoverBackgroundColor: barColours(da_hous)
      }]
    },
    options: defaults("Type of housing")
  });
}

// Type of tenure
var chart_tenure = function (props) {
  var cv_ten = document.getElementById("cv_ten").getContext('2d');
  var da_ten = [props.tenure_own, props.tenure_mor, props.tenure_ren];
  var ch_ten = new Chart(cv_ten, {
    type: 'bar',
    data: {
      labels: [["Owner-", "occupier"], ["Owner-", "occupier", "with", "mortgage"],
        "Renter"],
      datasets: [{
        data: da_ten,
        backgroundColor: barColours(da_ten),
        hoverBackgroundColor: barColours(da_ten),
        hoverBorderWidth: 2
      }]
    },
    options: defaults("Type of tenancy")
  });
}

// Type of tenure
var chart_household = function (props) {
  var cv_hh = document.getElementById("cv_hh").getContext('2d');
  var da_hh = [props.household_, props.household2, props.household3];
  var ch_hh = new Chart(cv_hh, {
    type: 'bar',
    data: {
      labels: ["Family", ["Sole", "household"], ["Group", "household"]],
      datasets: [{
        data: da_hh,
        backgroundColor: barColours(da_hh),
        hoverBackgroundColor: barColours(da_hh)
      }]
    },
    options: defaults("Household composition")
  });
}

var drawCharts = function (props) {
  chart_weekly_income(props);
  chart_age(props);
  chart_education(props);
  chart_housing(props);
  chart_tenure(props);
  chart_household(props);
}
