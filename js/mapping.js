// Init map
var map = L.map('map').setView([-20.1392784, 146.3292991], 5.5);
map.spin(true);
// initialise empty vars for controls later
var info; var profile;

// Colour maps according to party
function init_style(feature) {
    return {
        fillColor: partyColours(feature.properties.sitting_pa),
        weight: 1,
        opacity: 1,
        color: "#efefef",
        //dashArray: '3',
        fillOpacity: 0.4
    };
}

// Function to highlight polygons on mouseover

function highlightFeature(e) {
  var layer = e.target;

/*  layer.setStyle({
    weight: 5,
    color: '#666',
    dashArray: '',
    fillOpacity: 0.7
  }); */

  if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
    layer.bringToFront();
  }

  info.update(layer.feature.properties);
}

// When mouseoff, reset style
function resetHighlight(e) {
    districts.resetStyle(e.target);
}

// When click, zoom to the div
function zoomToFeature(e) {
  var layer = e.target;
  districts.eachLayer(function(e){districts.resetStyle(e)})
  layer.setStyle({
      fillOpacity: 0.8
    });
  profile.update(layer.feature.properties);
  info.update(layer.feature.properties);
  map.fitBounds(layer.getBounds());
  drawCharts(layer.feature.properties);
  addURLParameter(layer.feature.properties.NAME);
  $('.dropdown').val(divName(layer.feature.properties.NAME));
};

// Bring them together
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        //mouseout: resetHighlight,
        click: zoomToFeature,
    });
  layer._leaflet_id = feature.properties.NAME;
}

// add division polygons from QLD data
var districts = new L.GeoJSON.AJAX("data/State_electoral_boundaries_2017.geojson", {
  style: init_style,
  onEachFeature: onEachFeature
}).addTo(map);
districts.on("data:loaded", function() {

  var Divlist = function () {
    var divs = Object.keys(map._layers).splice(3);
    var opts = "";
    for (i = 0; i < divs.length; i++) {
      opts += "<option value='" + divName(divs[i]) + "'>" + divName(divs[i]) + "</option>"
    };
    return opts
  }

/*  divlist = L.control({position: 'topright'});
  divlist.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'divlist'); // create a div with a class "divlist"
    this._select = L.DomUtil.create('select', 'dropdown', this._div);
    this._select.innerHTML = Divlist();
    this._div.firstChild.onmousedown = this._div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return this._div;
  };
  divlist.addTo(map);
*/



  // Right-hand profile, with graphs generated on click
  profile = L.control({position: 'topright'});
  profile.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'profile'); // create a div with a class "profile"
    this._div.innerHTML = '<h2>Division profile</h2>'
    this._div.firstChild.onmousedown = this._div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    this._select = L.DomUtil.create('select', 'dropdown', this._div);
    this._select.innerHTML = Divlist();
    this._profile = L.DomUtil.create('div', '', this._div); // create a div with a class "profile"

    this.update();
    return this._div;
  };
  profile.update = function (props) {

    var profile = function (props) {
      if (props) {
        return "<canvas id='cv_wi' width='200' height='150'></canvas>" +
          "<canvas id='cv_a' width='200' height='150'></canvas>" +
          "<canvas id='cv_e' width='200' height='150'></canvas>" +
          "<canvas id='cv_hous' width='200' height='150'></canvas>" +
          "<canvas id='cv_ten' width='200' height='150'></canvas>" +
          "<canvas id='cv_hh' width='200' height='150'></canvas>"
      } else { return '<p>Click on a division.</p>' }
    };

    this._profile.innerHTML =
      profile(props) +
      '</div>'

  };
  profile.addTo(map);

  $('.dropdown').change(function() {
    var division = $('.dropdown').val().toUpperCase();
    var layer = map._layers[division];
    layer.fire("click");

  });

  if (getURLParameter('div') != null) {
    var division = getURLParameter('div').toUpperCase();
    var layer = map._layers[division];
    layer.fire("click");
  }
});

// Define OSM layer var
var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
    maxZoom: 14,
    minZoom: 4,
    attribution: 'Map tiles &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

// add OpenStreetMap tile layer
OpenStreetMap_BlackAndWhite.addTo(map);

// Adds attributiokkn
map.attributionControl.addAttribution('data from ' +
  '<a href="http://www.abs.gov.au/">Australian Bureau of Statistics</a> (under a <a href="https://creativecommons.org/licenses/by/2.5/au/">CC-BY 2.5 AU license</a>), and ' +
  '<a href="http://statistics.qgso.qld.gov.au/qld-regional-profiles">Queensland Treasury</a> (under a <a href="https://creativecommons.org/licenses/by/4.0/">CC-BY 4.0 license</a>)  . ' +
  'Developed by the <a href="https://www.griffith.edu.au/business-government/policy-innovation-hub">Policy Innovation Hub at Griffith University</a>. ' +
  '<a href="https://gitlab.com/pih/qldvotes">See the code at GitLab</a>.'
);




// Left-hand info, with text stats on mouseover
var info = L.control({position: 'topleft'});
info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};
// define contents of update
info.update = function (props) {
  var sitting = function(props) { // function to handle seats without members
    if (props.sitting_me == null) { // if seat is new (i.e., has no incumbent)
      // return a list with two elements:
      // first for the first line of the info control,
      // second for the last line
      return [
        "New seat after redistribution (<strong>" + props.sitting_pa + "</strong><sup>*</sup>)",
        "<P><sup>*</sup><small>Since there is no incumbent, " +
        "the party and margin given for each seat has " +
        "been calculated by " +
        "<a href='http://www.abc.net.au/news/elections/qld-redistribution-2017'>" +
        "Anthony Green at the ABC" + "</a>.</small></p>"
      ]
    } else {
      // return list as above, but without footnote
      return ["Sitting member: <strong>" + props.sitting_me + " (" + props.sitting_pa + ")</strong>",
        ""]
    };
  };

  // define function for the contents of info control;
  // function is preferred for allowing for function calls
  var infoContents = function() {
    return '<p>' +
      "<h3>" + props.NAME + "</h3>" +
      sitting(props)[0] + "<br/>" +
      "Seat held by: " + Math.round((props.held_by * 100) * 10)/10 + "%" + "<br/>" +
      "<br/>" +
      "Population: " + numberWithCommas(props.total_pers) + "<br/>" +
      "Median income: $" + numberWithCommas(props.median_per) + "<br/>" +
      "Median age: " + props.median_age + "<br/>" +
      '</p>' +
      sitting(props)[1]
  }
    this._div.innerHTML = (props ? infoContents() : 'Hover over an electoral division.');
};
info.addTo(map);

// Resize on window resize
$(window).on("resize", function() {
    $("#map").height($(window).height()).width($(window).width());
    map.invalidateSize();
}).trigger("resize");

// Numbers with commas
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// Div name strings
function divName(name) {
  return name.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

// return function to get div parameters
// (from https://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript)
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
function addURLParameter(name) {
  var param = "?div=" + divName(name);
  window.history.pushState("Policy Innovation Hub: Queensland Election Map", "", param)
}

function getShareLink(name)
{
  var baseurl = location.protocol + '//' + location.host + location.pathname;
  return baseurl + "?div=" + divName(name);
}

$(window).on("load", function() {
  map.spin(false);

});
