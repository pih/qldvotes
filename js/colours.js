function partyColours(party) {
    switch (party) {
      case "ALP": return "#DE2C34";
      case "LNP": return "#0047AB";
      case "ONP": return "#F8F16F";
      case "KAP": return "#b50204";
      case "IND": return "darkgray";
  }
}

function barColours(data) {
  var output = [];
  var max = Math.max(...data);
  for (i = 0; i < data.length; i++) {
    output.push("rgb(0, 0, " + (data[i]/max * 255) + ", 0.7)");
  }
  return output
}
